import zerorpc
import string
import pickle
import urllib2
import json
from word import Word
# from nltk.book import *

class Backend(object):
	_JAMESDOMAIN = "jamesctplant.co.uk"

	def __init__(self):
		try:
			self.words_i_know = pickle.load(open('./data/words.pkl', 'rb'))
		except:
			self.words_i_know = []

	def new_message(self, msg):
		if self._JAMESDOMAIN in msg['body']:
			url = 'https://' + self._JAMESDOMAIN + '/wp-json/wp/v2/posts?filter[posts_per_page]=1'
			latest_post = urllib2.urlopen(url).read
			readable_json = json.loads(latest_post)
			return readable_json['id']
		return "Nope"

	def save_words(self, msg):
		if '?' in msg['body']:
			is_question = True
		sentence = msg['body'].lower().translate(string.maketrans("",""), string.punctuation).split()
		index = 0
		for new_word in sentence:
			found = next((x for x in self.words_i_know if x.word == new_word), None)
			if not found:
				word = Word(new_word, sentence[index - 1])
				try:
					word.word_next = sentence[index + 1]
				except IndexError as e:
					word.word_next = None
					pass
				self.words_i_know.append(word)
				print "Added", new_word, "to dictionary!"
			index += 1
		# print sentence
		# print self.words_i_know
		pickle.dump(self.words_i_know, open('words.pkl', 'wb'))
		return "This is the word saving function!"

	def question_asked(self, msg):
		question = msg['body'].split()

def main():
	s = zerorpc.Server(Backend())
	s.bind('tcp://*:4242')
	s.run()

if __name__ == "__main__" : main()
